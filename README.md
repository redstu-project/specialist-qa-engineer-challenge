# Overview

This repository contains a simple API for managing notes. It uses postgresql as its database. For you to be able to
complete the challenge, you have to complete successfully the steps below.

# Installation

1- Clone the project to your local machine, and navigate to the root of the project.

2 - Install dependencies by running   
`python3 -m pip install -r requirements.txt`

3 - Run the database by running  
`docker-compose up`

4 - Run the api  
`uvicorn app.main:app --host 0.0.0.0 --port 8000 --reload`

5 - Open the link
`http://localhost:8000/docs#/`

If you are able to see the API specification titled FastAPI then you have completed the setup.